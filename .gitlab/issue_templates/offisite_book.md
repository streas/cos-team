From https://about.gitlab.com/handbook/ceo/offsite/#book-choice,

> The offsite includes a 1 hour discussion, 15 minutes of which is on action items, on a book read explicitly for the offsite.
> The CoS solicits book nominations from throughout the company into a Google Form.
> Then GitLab team members get to vote on the book.
> The book will be finalized no less than 1 month prior to the offsite.

**Please add books as individual comments to this issue** so that people can emoji vote.

## Timeline
[Dates included as sample- update.]
* 2020-04-05: Nominations issue opened.
* 2020-04-09: Nominations end, voting via emoji begins.
* 2020-04-17: Announcement made.
* 2020-05-18: Offsite begins

## Previous Reads

Previous reads can be found in the handbook --> https://about.gitlab.com/handbook/ceo/offsite/#previous-reads

## Previous Issues:
* 2020-05: https://gitlab.com/gitlab-com/cos-team/-/issues/22
* 2020-02: https://gitlab.com/gitlab-com/cos-team/-/issues/13
